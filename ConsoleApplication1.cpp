﻿#include <iostream>
#include <new>

using namespace std;

template <typename T>
class STACK
{
private:
    T* stack;
    int count;

public:
    STACK()
    {
        stack = nullptr;
        count = 0;
    }

    void push(T item)
    {
        T* temp;
        try {
            temp = stack;
            stack = new T[count + 1];
            count++;

            for (int i = 0; i < count - 1; i++)
                stack[i] = temp[i];

            stack[count - 1] = item;

            if (count > 1)
                delete[] temp;
        }
        catch (bad_alloc e)
        {
            cout << e.what() << endl;
        }
    }

    ~STACK()
    {
        if (count > 0)
            delete[] stack;
    }

    T pop()
    {
        if (count == 0)
            return 0;
        count--;
        return stack[count];
    }

    void Print()
    {
        T* p;
        p = stack;
        cout << "Stack: " << endl;
        if (count == 0)
            cout << "is empty." << endl;

        for (int i = 0; i < count; i++)
        {
            cout << "Item[" << i << "] = " << *p << endl;
            p++;
        }
        cout << endl;
    }
};

void main()
{
  
    STACK <int> st1;


    st1.Print();

    st1.push(2);
    st1.push(5);
    st1.push(1);

    st1.Print();

    st1.pop();

    st1.Print();

    st1.pop();

    st1.Print();

    st1.pop();

    st1.Print();

    st1.push(1);
    st1.push(5);
    st1.push(2);

    st1.Print();
};